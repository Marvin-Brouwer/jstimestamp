/* istanbul ignore file */
/* This file only contains a type and a non-testable function */

/**
 * A simple numerical representation of the Javascript @see Date object in UTC
 * similar to the Unix timestamp but including miliseconds.
 * @see https://gitlab.com/Marvin-Brouwer/jstimestamp#readme
 */
// The ' & { }' is to trick the compiler into seeing the timestamp as a type
export type JsTimestamp = number & {}

/**
 * Create a @see JsTimestamp of the current date and time in UTC
 */
export const stamp: () => JsTimestamp = Date.now