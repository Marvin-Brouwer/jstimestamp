import { isNullOrUndefined, isDate } from '@delta-framework/core'
import { JsTimestamp } from './JsTimestamp'
import { isJsTimestamp } from './Validations'

/**
 * Convert a @see Date to a @see JsTimestamp
 * @param date javascript Date object
 * @returns a @see JsTimestamp
 * @throws EvalError 'The object provided is not a valid Date'
 */
export const toJsTimeStamp = (date: Date): JsTimestamp => {

    if (isNullOrUndefined(date))
        throw new ReferenceError('The date argument is required')
    if (!isDate(date))
        throw new EvalError('The object provided is not a valid Date')

    return +date
}

export const toDate = (jsTimestamp: JsTimestamp): Date => {

    if (isNullOrUndefined(jsTimestamp))
        throw new ReferenceError('The jsTimestamp argument is required')
    if (!isJsTimestamp(jsTimestamp))
        throw new EvalError('The jsTimestamp provided is not a valid JsTimestamp')

    return new Date(jsTimestamp)
}