import { isNullOrUndefined } from '@delta-framework/core'
import { JsTimestamp } from './JsTimestamp'

/**
 * Validate whether or not a value is a valid @see JsTimestamp
 * @param value JsTimestamp candidate
 * @see timeStampPattern
 */
export const isJsTimestamp = (value: number | string): value is JsTimestamp => {

    if (isNullOrUndefined(value)) return false
    if (Number.isNaN(+value)) return false
    if (`${value}`.length < 13) return false

    return true
}