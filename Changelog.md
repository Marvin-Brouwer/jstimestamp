# Changelog

## Release 1.0.0 (latest)
* Simplified timestamp
* Automated build process
* Added tests
* Added documentation
* Supporting newer engines
* Split up code file

## Initial version (unreleased)
* Developed in another project