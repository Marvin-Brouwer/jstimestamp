import test from 'ava'

import { toDate } from '../lib/Conversions'

test('is null throws', assert => {

    // Assign
    // tslint:disable-next-line:no-null-keyword
    const timestamp: any = null

    // Act
    const result = () => toDate(timestamp)

    // Assert
    assert.throws(result)
})

test('is undefined throws', assert => {

    // Assign
    const timestamp: any = undefined

    // Act
    const result = () => toDate(timestamp)

    // Assert
    assert.throws(result)
})

test('is invalid throws', assert => {

    // Assign
    const timestamp = 123

    // Act
    const result = () => toDate(timestamp)

    // Assert
    assert.throws(result)
})

test('is valid returns date', assert => {

    // Assign
    // Fri Oct 19 2018 19:45:28 GMT+0000
    const timestamp = 1539978328000

    // Act
    const result = toDate(timestamp)

    // Assert
    assert.is(result.toUTCString(), 'Fri, 19 Oct 2018 19:45:28 GMT')
})