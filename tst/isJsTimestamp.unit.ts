import test from 'ava'

import { isJsTimestamp } from '../lib/Validations'

test('is timestamp returns true', assert => {

    // Assign
    const timestamp1 = 1539978328000
    const timestamp2 = '1539978328000'

    // Act
    const result1 = isJsTimestamp(timestamp1)
    const result2 = isJsTimestamp(timestamp2)

    // Assert
    assert.is(result1, true)
    assert.is(result2, true)
})

test('isn´t timestamp returns false', assert => {

    // Assign
    const timestampA = '1539978328000aaa'
    const timestampB = 123

    // Act
    const resultA = isJsTimestamp(timestampA)
    const resultB = isJsTimestamp(timestampB)

    // Assert
    assert.is(resultA, false)
    assert.is(resultB, false)
})

test('is null or undefined returns false', assert => {

    // Assign
    // tslint:disable-next-line:no-null-keyword
    const timestampA: any = null
    const timestampB: any = undefined

    // Act
    const resultA = isJsTimestamp(timestampA)
    const resultB = isJsTimestamp(timestampB)

    // Assert
    assert.is(resultA, false)
    assert.is(resultB, false)
})