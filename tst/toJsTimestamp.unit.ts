import test from 'ava'

import { toJsTimeStamp } from '../lib/Conversions'
import { emptyString } from '@delta-framework/core'

test('is null throws', assert => {

    // Assign
    // tslint:disable-next-line:no-null-keyword
    const date: any = null

    // Act
    const result = () => toJsTimeStamp(date)

    // Assert
    assert.throws(result)
})

test('is undefined throws', assert => {

    // Assign
    const date: any = undefined

    // Act
    const result = () => toJsTimeStamp(date)

    // Assert
    assert.throws(result)
})

test('not Date throws', assert => {

    // Assign
    const date: any = emptyString

    // Act
    const result = () => toJsTimeStamp(date)

    // Assert
    assert.throws(result)
})

test('is Date returns timestamp', assert => {

    // Assign
    const date = new Date('Fri Oct 19 2018 19:45:28.000 GMT')

    // Act
    const result = toJsTimeStamp(date)

    // Assert
    // Fri Oct 19 2018 19:45:28 GMT+0000
    assert.is(result, 1539978328000)
})