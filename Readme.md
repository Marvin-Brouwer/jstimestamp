[//]: # (Header)

<div align="center">
  <a  href="https://gitlab.com/Marvin-Brouwer/jstimestamp#readme" 
      align="center" >
      <img  width="250" height="304" 
            align="center" alt="JsTimestamp" title="JsTimestamp" 
            src="https://gitlab.com/Marvin-Brouwer/jstimestamp/raw/master/resource/banner.png?inline=false" />
  </a>
</div>
  
[![License][license-image]][license-url] 
[![NPM version][npm-version-image]][npm-package-url] 
[![Downloads][npm-downloads-image]][npm-package-url]  
[![Build Status][build-image]][build-url] 
[![Coverage][coverage-image]][build-url]  
  
[//]: # (Documentation)
  
```plaintext
npm install jstimestamp@latest
```
  
## Summary  
  
This is an abstraction of the JavaScript [timestamp][wiki-timestamp] with some validation created to share the standard [Date][mdn-date] object between browser and server without large libraries. It's basically [Date.now()][mdn-date-now] with a custom type and validation  
If you're looking for an advanced [Date][mdn-date] library please checkout: [Luxon][luxon] or [momentjs][momentjs]. This library does not contain any timezone manipulation.  
  
## Usage  
  
The library exposes a type which is basically an alias for a string, this is just for clarity while developing.  
Also there are tree methods included:
* **isJsTimestamp**: Validates values to be a valid  JsTimestamp
* **toJsTimestamp**: Converts [Date][mdn-date] objects to a JsTimeStamp
* **stamp**: Date.now but casted to a JsTimestamp
* **toDate**: Convert a JsTimestamp to a [Date][mdn-date]  
  
## [Contributing][contributing-url]  
[contributing-url]: https://gitlab.com/Marvin-Brouwer/jstimestamp/blob/master/Contributing.md
  
See the [Contribution guide][contributing-url] for help about contributing to this project.  
  
## [Changelog][changelog-url]  
[changelog-url]: https://gitlab.com/Marvin-Brouwer/jstimestamp/blob/master/Changelog.md
  
See the [Changelog][changelog-url] to see the change history.  
  
[//]: # (Labels)

[mdn-date]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
[mdn-date-now]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/now

[luxon]: https://moment.github.io/luxon/
[momentjs]: https://momentjs.com/
[wiki-timestamp]: https://en.wikipedia.org/wiki/Timestamp

[npm-package-url]: https://www.npmjs.com/package/jstimestamp
[npm-version-image]: https://img.shields.io/npm/v/jstimestamp.svg?style=flat-square
[npm-downloads-image]: https://img.shields.io/npm/dm/jstimestamp.svg?style=flat-square

[license-url]: https://gitlab.com/Marvin-Brouwer/jstimestamp/blob/master/License.md#blob-content-holder
[license-image]: https://img.shields.io/badge/license-Apache--2.0-blue.svg?style=flat-square
[build-url]: https://gitlab.com/Marvin-Brouwer/jstimestamp/pipelines
[build-image]: https://gitlab.com/Marvin-Brouwer/jstimestamp/badges/master/build.svg?style=flat-square
[coverage-image]: https://gitlab.com/Marvin-Brouwer/jstimestamp/badges/master/coverage.svg?style=flat-square